package com.uca.capas.tarea3.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MainController {
	
	@RequestMapping("/ingresar")
	public String index() {
		return "tarea3/index";
	}
	
	@RequestMapping("/parametros1")
	public ModelAndView parametros1(HttpServletRequest request) throws ParseException {
		ModelAndView mav = new ModelAndView();
		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");	
		String fechanac = request.getParameter("fechanac");	
		String lugarnac = request.getParameter("lugarnac");
		String colegio = request.getParameter("colegio");
		String telefono = request.getParameter("telefono");
		String celular = request.getParameter("celular");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date d1 = sdf.parse(fechanac);
		Date d2 = sdf.parse("2003-01-01");
		
		System.out.println(fechanac);
		
		List<String> alumno = new ArrayList<>();
		String alumnoCorrecto = "Alumno ingresado con éxito";
		
		//Nombre
		if(nombre.length()<1 || nombre.length()>25) {
			alumno.add("+ Mayor a 1 y Menor a 25 su Nombre");
		}
		//Apellido
		if(apellido.length()<1 || apellido.length()>25) {
			alumno.add("+ Mayor a 1 y Menor a 25 su Apellido");
		}	
//		Fecha de Nacimiento
		if(d1.before(d2)) {
			alumno.add("+ La Fecha debe ser mayor al 1 de Enero del 2003");
		}	
		
		//Lugar de Nacimiento
		if(lugarnac.length()<1 || lugarnac.length()>25) {
			alumno.add("+ Mayor a 1 y Menor a 25 su Lugar de Nacimiento");
		}
		//Colegio
		if(colegio.length()<1 || colegio.length()>100) {
			alumno.add("+ Mayor a 1 y Menor a 100 su Colegio");
		}
		//Telefono
		if(telefono.length()!= 8) {
			alumno.add("+ Debe ser igual a 8 Tel");
		}
		//Celular
		if(celular.length()!= 8) {
			alumno.add("+ Debe ser igual a 8 Cel");
		}

		
		if(alumno.isEmpty()) {
			mav.addObject("mensaje", alumnoCorrecto);
			mav.setViewName("tarea3/agregado");

		}else {
			mav.addObject("errores", alumno);
			mav.setViewName("tarea3/errores");
		}
		
		
		return mav;
		
	}//En mi commit anterior habia usado las validaciones de Java pero no supe bien como implementarlas dentro de un if
	
// kill 8080 port linux: sudo netstat -lpn |grep :8080

}
