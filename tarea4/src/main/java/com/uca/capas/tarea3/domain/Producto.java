package com.uca.capas.tarea3.domain;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class Producto {
	@Pattern(regexp = "^[0-9][0-9]{11}", message = "Debe contener 12 digitos" )
	String codigo;
	@Size(min = 1, max = 100, message = "Nombre debe contener de 1 a 100 caracteres")
	String nombre;
	@Size(min = 1, max = 100, message = "Marca debe contener de 1 a 100 caracteres")
	String marca;
	@Size(min = 1, max = 500, message = "Descripcion debe contener de 1 a 500 caracteres")
	String descripcion;
	@Pattern(regexp = "^[0-9]{0,5}", message = "Debe contener solo enteros" ) //Lo habia declarado con @Digits pero no se muy bien como aplicarlo todavia agradeceria si pudiera dar la solucion a su forma de esta tarea gracias
	String existencias;
	@Pattern(regexp = "^(3[01]|[12][0-9]|0[1-9])/(1[0-2]|0[1-9])/[0-9]{4}$", message = "Debe ser de tipo dd/MM/yyyy")
	String fecha;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getExistencias() {
		return existencias;
	}
	public void setExistencias(String existencias) {
		this.existencias = existencias;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public Producto(String codigo, String nombre, String marca, String descripcion, String existencias, String fecha) {
		super();
		this.codigo = codigo;
		this.nombre = nombre;
		this.marca = marca;
		this.descripcion = descripcion;
		this.existencias = existencias;
		this.fecha = fecha;
	}
	public Producto() {	
	}
	
	
	
	
}
