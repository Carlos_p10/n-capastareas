package com.uca.capas.tarea3.controller;

import java.text.ParseException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import com.uca.capas.tarea3.domain.Producto;

@Controller
public class MainController {
	
	@RequestMapping("/producto")
	public ModelAndView index() {
		ModelAndView mav = new ModelAndView();
		mav.addObject("producto", new Producto());
		mav.setViewName("tarea3/index");
		return mav;
	}

	@RequestMapping("/procesar")
	public ModelAndView procesar2(@Valid @ModelAttribute Producto pro, BindingResult result) {
		ModelAndView mav = new ModelAndView();
		
		if(result.hasErrors()) { 
			mav.setViewName("tarea3/index");
		}
		else { 
			mav.addObject("nombre", pro.getNombre());
			mav.setViewName("tarea3/agregado");
		}
		return mav;
	}
// kill 8080 port linux: sudo netstat -lpn |grep :8080

}
