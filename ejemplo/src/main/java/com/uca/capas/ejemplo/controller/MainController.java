package com.uca.capas.ejemplo.controller;

import java.util.Calendar;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MainController {

	@RequestMapping("/alumno")
	public @ResponseBody String alumno() {
		return "Carlos, Paredes, 00028516, Ing. Informatica, 5º Año";
	}
	
	@RequestMapping("/fecha")
	public @ResponseBody String parametro(HttpServletRequest request) {
		int dia  = Integer.parseInt(request.getParameter("dia"))-1;
		int mes = Integer.parseInt(request.getParameter("mes"))-1;
		int anio =  Integer.parseInt(request.getParameter("anio"));
		
		Calendar c = Calendar.getInstance();
		c.set(anio, mes, dia, 0, 0);
		String diaString;
		System.out.println( "HHHHHHHH DAY" + c.get(Calendar.DAY_OF_WEEK ) + c.get(Calendar.MONTH) + c.getTime());
		
		switch (c.get(Calendar.DAY_OF_WEEK )) {
			case 1: diaString = "Lunes";
			break;
			case 2: diaString = "Martes";
			break;
			case 3: diaString = "Miercoles";
			break;
			case 4: diaString = "Jueves";
			break;
			case 5: diaString = "Viernes";
			break;
			case 6: diaString = "Sabado";
			break;
			case 7: diaString = "Domingo";
			break;
			default: diaString = "Ningun dia seleccionado";
			break;
		}
		return "Dia de la semana ingresado: " + diaString;
		
		//http://localhost:8080/fecha?dia=4&mes=1&anio=2019 ejemplo
	}
}
